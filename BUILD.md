# How To Build

## Prerequisites
```
# Debian
apt-get install nodejs git
```
```
# CentOS
yum install nodejs git
```
```
# Fedora
dnf install nodejs git
```
Or see [NodeJS Documentation](https://nodejs.org/en/download/package-manager/) for distro-specific instructions.

## Install GitBook
```
npm install gitbook-cli -g
```
This step MAY require `sudo` depending on your system and NodeJS installation method.

## Pull Dependencies of Our Book
```
cd $REPO
gitbook install
```

## Start and Happily Edit
```
cd $REPO
gitbook serve
```
> Edits will be show live in your browser window, no need to refresh.
