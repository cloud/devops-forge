# Containers
`TODO`

## Prerequisites
* [Linux](../0-linux/README.md)
  * OS installation
  * Networking
  * Shell (`bash`)

## Outline
This µ-course aims to introduce:
- concepts and terminology used in containers,
- selected implementations and tools,
- basic skills needed for running containers and building container images.

Topics include:
- differences between containers and virtual machines,
- kernel namespaces,
- installation,
- Docker usage,
- Dockerfile syntax and usage.

## External Sources

* [Docker tutorial](https://docs.docker.com/get-started/)
* [Docker-compose tutorial](https://docs.docker.com/compose/gettingstarted/)

`TODO`
