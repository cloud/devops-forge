# Questions

## Docker
1. What's the difference between docker image and docker container?
2. What container orchestration platform(s) can you name?
{% reveal %}
```
Kubernetes, Docker Swarm, Apache Mesos, ...
```
{% endreveal %}

3. How would you create a container from the latest Apache (httpd) image and bind it to host's port 80?
{% reveal %}
```
docker run -dit -p 80:80 httpd
```
{% endreveal %}