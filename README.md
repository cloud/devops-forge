# Introduction
DevOps forge is a place where the young DevOps engnineers of steel are made. 
The aim of this site is to provide you with information relevant for our work.
The core structure of this site is organized around the 5 basic themes, such as
1. [Linux](0-linux)
2. [Versioning Control System](1-vcs)
3. [Virtualization](2-virtualization)
4. [Container](3-containers)
5. [Configuration Management](4-cfgmngmnt)

Each section has the same structure 
* Owerview 
* Questions 
* Tasks 

In the end of each section you should have complex overview of our work and 
you should be qualified to become a valued member of our DevOps team.

As this DevOps forge site is mainly focused on the technicalities, we can imagine that 
some practical information would be needed too.

## Practical information

 Employees of [ICS MUNI](https://gitlab.ics.muni.cz/OSS/intro/blob/master/ICS_INFO_EN.md)

 Employees of [CESNET](https://wiki.cesnet.cz/doku.php?id=wiki:tapajici_zamestnanec)

