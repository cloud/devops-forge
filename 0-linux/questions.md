# Questions
### When appropriate, choose one or more correct answers.
1. Changes to environment variables by a child process are not seen by the parent. (True/False)
2. In Linux, the current directory is automatically in your search path. (True/False)
3. What would be output of the following comand:
`echo "aa_bb" | sed "s/.*_\(.*\)/\1/"`
4. What would be output of the following command sequence:
`echo 1; echo $?`
5. Write a simple script in your favorite script language that would sum up all numbers from file 'numbers.txt'. The numbers in the file are listed one per line.