# Linux
Linux is a very hands-on operating system. If running Windows is like driving an automatic, then running Linux is like driving a stick. 
It can take some work, but once you know your way around Linux, you’ll be using the command line and installing packages like a pro.

## Prerequisites
* Basic knowledge of Linux, strong motivation and access to a computer that can run Linux
* OS installation
* Shell (`bash`)
* Remote access (`ssh`)

1. Go through the recommended external sources (in the bottom part of this site)
2. Take the [test](questions.md)
3. Finish the [task](tasks.md)

## Outline
This µ-course aims to introduce:
- concepts and terminology used in Linux-based operating systems,
- selected Linux subsystems and their purpose,
- basic skills needed for installing and using Linux-based operating systems.

Topics include:
- shell,
- process management,
- file systems,
- memory management,
- network management,
- system installation,
- package management,
- security best practices,
- remote access.

## External Sources
* [General concepts](https://www.linode.com/docs/tools-reference/introduction-to-linux-concepts/) - it is highly recommended to go through all the embedded links in the text
* [Shell, File systems, Commandline tools](https://www.fi.muni.cz/usr/brandejs/PV004/)
* [Package management](https://www.linode.com/docs/tools-reference/linux-package-management/)
