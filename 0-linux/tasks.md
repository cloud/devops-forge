# Tasks

## Task 1
Delete file `-Z`.

## Task 2
Prepare a bash script, which inserts text into the file **kokos.txt**, while the text format remains unchaged.
```yaml
# Hiera 5 Global configuration file
version: 5
defaults:
  datadir: ../code/environments/production/hieradata
    data_hash: yaml_data
    hierarchy:
      - name: Configuration hierarchy
          paths:
                - "hiera_data.yaml"
```

## Task 3
Switch on VIM and try to exit [it].

## Task 4
Install package `sl`.

