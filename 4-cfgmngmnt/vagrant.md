# Vagrant
Aim of this µ-course is to teach our students basic skills for running, updating and creating virtual machine environments using Vagrant and underlying VirtualBox virtualization platform. This µ-course will be mostly pointing to https://www.vagrantup.com/ tutorial and contain mainly tasks for students to complete.

## Expected previous knowledge
* linux basics
  * ssh configuration
  * basic networking
  * bash scripting
* git

## Expected chapters
* Introduction:
  * What is Vagrant
  * Why we use it
* Installation
    * How to install Vagrant on linux machine
* Tasks
  * Vagrant basics (`up`, `ssh`, `halt`, `destroy`)
  * Vagrantfile for single Vagrant box
    * provisioning scripts (bash)
    * example use of ansible and puppet provisioning
  * Vagrantfile for multiple Vagrant boxes
    * networking
  * Vagrant common plugins
    * vagrant-vbguest, vagrant-hosts, vagrant-timezone, vagrant-cahier
