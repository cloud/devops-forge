# Tasks

## Vagrant
Tasks are separated from easy to hard. You don't need to make them all, just one (ore two if you want). Harder tasks are better ;-). Results should be handed over as a link to your repository at [Gitlab](https://gitlab.ics.muni.cz) or [Github](https://github.com/) with your solution and solution description in `README.md` file.

### Low difficulty
* create Vagrantfile for single machine with the following software installed: `tmux`, `htop`

### Medium difficulty
* create Vagrantfile for single machine with the following software installed
  * `tmux` installed though Shell provider
  * `htop` installed though Ansible provider
  * `mc` installed though Puppet provider


* create Vagrantfile which creates virtual machines `server` and `host`
  * `server` provides small network storage for `host`
  * `host` communicates with server over private network and has `server`'s network storage mounter to `/data`

### Hard difficulty
* create Vagrantfile which creates virtual machines `server` and variable number of `host<num>` machines defined by configuration of your choice
  * `server` provides private network storage for every `host<num>`
  * each `host<num>` communicates with server over private network and has `server`'s network storage mounter to `/data`
  * Each host is accesible from 147.251.0.0/16 network via ssh on port of your choice.
  * ssh accepts connections only with public key defined in configuration

## Ansible
Tasks are separated from easy to hard. You don't need to make them all, just one (ore two if you want). Harder tasks are better ;-). Results should be handed over as a link to your repository at [Gitlab](https://gitlab.ics.muni.cz) or [Github](https://github.com/) with your solution and solution description in `README.md` file.

### Low difficulty
* create a playbook which ensures (on localhost) that
  * `tmux`, `htop` packages are installed
  *  user `johnny` is present
  *  directory `/mydata/stuff` exists and contains empty file `johnnys_stash` is present
  * `johnnys_stash` is accesible only by `johnny`

### Medium difficulty
* create a playbook which ensures (on localhost) that
  * `tmux`, `htop` packages are installed using custom role `common_packages`
  *  user `johnny` is present using custom role `common_users`
  *  directory `/mydata/stuff` using custom role `common_stuff`
  * `johnnys_stash` is accesible only by `johnny` using single task in playbook

### Hard difficulty
* create Vagrantfile which creates virtual machines `server` and `host`. Use Ansible provider to configure the machines so that:
  * `server` and `host` contain user `johnny`
  * `johnny` has his ssh key on both servers and is able to ssh from one to another through private network by invoking a command `ssh host` or `ssh server`

* create Ansible module `custom_figpucker` with optional states: `absent` or `present`
  * when seleted state is `present`, machine on which is module applied will boot correctly only on every even run.
  * when seleted state is `absent`, the behavior described above is removed.

## Puppet
* Using modules and puppet forge
* Writing manifest with resource types 'exec, package, file, service'
* Defining a classes with/without parameters
