# Questions

## Vagrant
1. What is Vagrant?
{% reveal %}
```
Tool for building and managing virtual machine environments (in a single workflow).
```
{% endreveal %}

2. Why would you use Vagrant instead of creating testbed directly in VirtualBox?
{% reveal %}
```
Reproducibility and portability.
```
{% endreveal %}

## Ansible
1. What is Ansible?
{% reveal %}
```
Ansible is open source software that automates software provisioning, configuration management, and application deployment.
Ansible usually connects via SSH.
```
{% endreveal %}

2. Does Ansible support encryption of configuration files? If yes, how?
{% reveal %}
```
Yes, through Ansible vault (symmetric encryption).
```
{% endreveal %}

3. How is Ansible code structured?
{% reveal %}
```
Into playbooks, tasks, roles and inventory list + others, this is the basic stuff.
```
{% endreveal %}

4. What is Ansible playbook?
{% reveal %}
```
Playbooks are YAML files that express configurations, deployment, and orchestration in Ansible.
They allow Ansible to perform operations on managed nodes. Each Playbook maps a group of hosts
to a set of roles. Each role is represented by calls to Ansible tasks.
```
{% endreveal %}

5. What is Inventory (in Ansible)?
{% reveal %}
```
The Inventory is a description of the nodes that can be accessed by Ansible.
By default, the Inventory is described by a configuration file, in INI or YAML format.
```
{% endreveal %}

## Puppet
1. What is Puppet? What is Puppet Kick?
2. Can you describe differences between Puppet and Ansible?
3. What is a module and how is it different from a manifest? What is Facter?
4. Describe the most significant gain from automating a process through Puppet?
