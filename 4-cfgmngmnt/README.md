# Configuration Management
`TODO`

## Prerequisites
* [Linux](../0-linux/README.md)
* [Source Code Management](../1-vcs/README.md)

## Outline
* [Ansible](ansible.md)
* [Puppet](puppet.md)
* [Vagrant](vagrant.md)
---
* [Tasks](tasks.md)
* [Questions](questions.md)

## External Sources
`TODO`
