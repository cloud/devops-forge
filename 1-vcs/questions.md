# Questions

## Git
1. What is `git` and how does it differ from `svn`?
2. What are merge conflicts and how do you resolve them?
3. How would you drop all local changes in your working directory?
{% reveal %}
```
git reset --hard HEAD
```
{% endreveal %}
