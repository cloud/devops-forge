# Source Code Management
`TODO`

## Prerequisites
* [Linux](../0-linux/README.md)
  * Shell (`bash`)
  * Remote access (`ssh`)

## Outline
This µ-course aims to introduce:
- concepts and terminology used in source code management and versioning,
- selected implementations and tools,
- basic skills needed for source code versioning in small projects.

Topics include:
- centralized vs. distributed version control systems,
- basic code manipulation,
- team workflow.

## External Sources

* [Github Guides](https://guides.github.com/)
* [Gitlab tutorial](https://docs.gitlab.com/ee/gitlab-basics/)
