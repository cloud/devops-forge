# Tasks

## VirtualBox

### Create a Virtual Machine
1. Download CentOS 7 distribution [ISO](https://www.centos.org/download/).
2. Create an empty virtual machine (1CPU, 1GB ram, 10GB disk).
3. Install CentOS 7 as the operating system.
4. Enjoy, this is the last time you will have to install it manually.

### Task 2
`TODO`

### Task 3
`TODO`

## QEMU and KVM
`TODO`
