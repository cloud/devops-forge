# Virtualization
`TODO`

## Prerequisites
* [Linux](../0-linux/README.md)
  * OS installation
  * Networking
  * Remote access (`ssh`)
  * Shell (`bash`)

## Outline
This µ-course aims to introduce:
- concepts and terminology used in virtualization,
- selected implementations and tools,
- basic skills needed for running virtual machines.

Topics include:
- CPU and memory virtualization,
- IO virtualization,
- disk formats,
- installation,
- VirtualBox usage,
- QEMU and KVM usage.

## External Sources
`TODO`
