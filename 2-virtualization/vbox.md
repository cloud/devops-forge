# VirtualBox

## Introduction

### What
* Virtualization platform for desktops,
* multi-platform (and Linux-friendly),
* free to play with,
* similar to VMware Workstation or HyperV.

### Why
* Supports machines with different operating systems,
* creates clean environment,
* builds multi-node virtual environments,
* uses full virtualization,
* supports network without infrastructure-level anti-spoofing protections,
* is compatible with Vagrant,
* has a nice GUI.

### Limits
* Perfect for developers, but not for production.
* Performance and quantity of virtual machines is limited by desktop's RAM/CPU/disk.

### Installation
1. Update your system before installation and reboot to the latest kernel (if updated).
2. Install matching kernel headers and required packages.
  ```
   # RedHat
   $ yum install -y binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel-$(uname -r) dkms

   # Ubuntu/Debian
   $ apt-get -y install gcc make linux-headers-$(uname -r) dkms
  ```
3. Download and install VirtualBox and VirtualBox Extension Pack from https://www.virtualbox.org/wiki/Downloads.

> Avoid installation through the system package manager since VirtualBox packages are usually waaaaay outdated there.

### Known issues

* VT-x/AMD-V hardware acceleration
  ```
   # Error:
   # VT-x/AMD-V hardware acceleration is not available on your system

   # Fix:
   # Make sure you have virtualization support enabled in BIOS:

   # For Intel in BIOS ensure the following
   # Intel(R) Virtualization Technology [Enabled]

   # For AMD in BIOS ensure the following
   # ???
  ```

* Missing VirtualBox kernel modules
  ```
  # Error:
  # Kernel driver not installed (rc=-1908)
  # The VirtualBox Linux kernel driver (vboxdrv) is either not loaded or there is a permission problem with /dev/vboxdrv.
  # Please reinstall the kernel module by executing '/sbin/vboxconfig' as root.

  # Fix:
  # Check kernel version you are currently running
  $ uname -r
  4.16.3-301.fc28.x86_64

  # Install matchig kernel headers, for me at Fedora it looks like this:
  # RedHat
  $ yum install kernel-devel-$(uname -r)

  # Ubuntu/Debian
  $ apt install linux-headers-$(uname -r)

  # Re-install VirtualBox kernel modules by running
  $ /sbin/vboxconfig
  ```
