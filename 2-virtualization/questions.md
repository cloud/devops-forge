# Questions

## VirtualBox
1. What is VirtualBox?
{% reveal %}
```
Hypervisor. It enables you to run virtual machines.
```
{% endreveal %}

2. Why would you use VirtualBox?
{% reveal %}
```
Local testing that requires clean or different operating system.
Provisioning testing (from disk or PXE) that requires full boot process.
Low level networking tests (software defined network testbeds).
When using Vagrant (since it is free and fairly common).
```
{% endreveal %}

3. Why would you use a virtual machine with fixed-size disk instead of dynamically allocated disk?
{% reveal %}
```
Fixed-size disk is generally faster than a dynamically allocated one.
```
{% endreveal %}

4. How would you expose virtual machine's ssh port to be accessible from the internet?
{% reveal %}
```
NAT network and port forwarding.
Using bridged network adapter.
```
{% endreveal %}

5. What is a virtual machine snapshot and why would you use it?
{% reveal %}
```
Snapshot is a copy of the virtual machine's disk at a given point in time.
It is useful for test purpouses (when you expect upcoming roll-backs) or for backups.
```
{% endreveal %}

## QEMU and KVM
`TODO`
